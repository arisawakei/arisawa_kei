<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
<body>
<div class = "main-contents">
			<div class="menu">
			<a href ="http://localhost:8080/arisawa_kei/newPost">新規投稿</a>
			<c:if test = "${loginUser.branch_id ==1 && loginUser.department_id == 1}">
			<a href ="http://localhost:8080/arisawa_kei/manageUser">ユーザー管理</a>
			</c:if>
			<div class="log-out"><a href="logout">ログアウト</a></div>
			</div>
<h1>ユーザー編集</h1>
<a href = "./">ホーム</a> > <a href = "./manageUser">ユーザー管理</a> > ユーザー編集
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>
<form action="editUser" method="post"><br />

<input name ="id" value="${editUser.id}" id="id" type="hidden"/>

<div class="accountForm">
<label for="account">ログインID:</label>
<input name="account" value="${editUser.account}"id="account" />(6文字以上20文字以内)
</div>
<div class="passwordForm">
<label for="password">パスワード:</label>
<input name="password" type="password" id="password"/>(6文字以上20文字以内)
</div>
<div class="passwordCheckForm">
<label for ="passwordCheck">パスワード(確認用):</label>
<input name ="passwordCheck" type ="password">(6文字以上20文字以内)
</div>
<div class="nameForm">
<label for="name">名前:</label>
<input name="name" value="${editUser.name}" id="name">(10文字以内)
</div>

<c:if test = "${loginUser.id != editUser.id}">
<div class="branchForm">
<label for ="branch_id">支店:</label>
</div>
<div class="selected">
<select name = "branch_id">
<c:forEach items="${branchLists}" var="branchList">
<option value ="${branchList.id}" label= "${branchList.name}" <c:if test="${editUser.branch_id == branchList.id}">selected</c:if>  />
</c:forEach>
</select>
</div>

<div class="departmentForm">
<label for ="department_id">部署・役職:</label>
</div>
<div class="selected">
<select name = "department_id">
<c:forEach items="${departmentLists}" var="departmentList">
<option value ="${departmentList.id}" label= "${departmentList.name}" <c:if test="${editUser.department_id == departmentList.id}">selected</c:if>  />
</c:forEach>
</select>
</div>

</c:if>

<c:if test="${loginUser.id == editUser.id}">
	<c:forEach items="${branchLists}" var="branchList">
		<c:if test="${editUser.branch_id == branchList.id}">
			<input name="branch_id" value="${branchList.id}" id="branch_id"
				type="hidden" />
		</c:if>
	</c:forEach>


<c:forEach items="${departmentLists}" var="departmentList">
	<c:if test="${editUser.department_id == departmentList.id}">
		<input name="department_id" value="${departmentList.id}"
			id="department_id" type="hidden" />
	</c:if>
</c:forEach>
</c:if>
<div></div>
<div class="change"><input type = "submit" value ="変更" /></div>


</form>
		<div class="copyright">
			<br />Copyright(c) Kei Arisawa
		</div>
</div>

</body>
</html>