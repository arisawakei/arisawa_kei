<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
<script type="text/javascript">
<!--

function check1(){
	if(window.confirm('投稿を削除しますか？')){
		return true;
	} else {
		window.alert('キャンセルしました');
		return false;
	}
}
function check2(){
	if(window.confirm('コメントを削除しますか？')){
		return true;
	} else {
		window.alert('キャンセルしました');
		return false;
	}
}
// -->
</script>
</head>
<body>
	<div class="main-contents">
			<div class="menu">
			<a href ="http://localhost:8080/arisawa_kei/newPost">新規投稿</a>
			<c:if test = "${loginUser.branch_id ==1 && loginUser.department_id == 1}">
			<a href ="http://localhost:8080/arisawa_kei/manageUser">ユーザー管理</a>
			</c:if><div class="log-out"><a href="logout">ログアウト</a></div></div>
				<h1>社内掲示板</h1>
			<form action="./" method="get">
			<input name="from" id="from" value="${Search.from}" type="date"> ～
			<input name="to" id ="to" value="${Search.to}" type="date">
			<input name="category" id="category" value="${Search.category}" type="text">
			<input type="submit" value="検索"></form>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
							<c:forEach items="${errorMessages}" var="message">
								<br /><li><c:out value="${message}" />
							</c:forEach>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
			<div></div><div class="posts">
			<c:forEach items="${posts}" var="post">
				<div class="post">
				<font size ="3">
					<br /><b><div class="title">
						<b><c:out value="${post.title}" /></b>
						：＜<c:out value="${post.name}" />
						<fmt:formatDate value="${post.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />＞
						(カテゴリー：
						<c:out value="${post.category}" />)</b>
					</div>
					<div class="text">
						<c:forEach var="text" items="${ fn:split(post.text,'
						')}">
							<c:out value="${text}" /><br>
						</c:forEach>
					</div>
					<c:if test = "${loginUser.id == post.userId }">
					<form action="./index.jsp" method="post" onSubmit="return check1()">
						<input name="deletePost" value="${post.id}" id="deletePost"
							type="hidden" /> <input type="submit" value="投稿を削除">
					</form>
					</c:if>
				</div>
				<div class="comments">
					<font size="3"> <c:forEach items="${comment}" var="comment">
							<div class="comments">
								<c:if test="${post.id == comment.postId}">
							<div class="show-comments">
								<b>コメント ：＜<c:out value="${comment.name}" /> <fmt:formatDate
										value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />＞</b>
								<div class="text">

									<c:forEach var="comments"
										items="${ fn:split(comment.text,'
								') }">
										<c:out value="${comments}" />
										<br>
									</c:forEach>
								</div>
								<c:if test="${loginUser.id == comment.userId }">
									<form action="./index.jsp" method="post"
										onSubmit="return check2()">
										<input name="id" value="${comment.id}" id="id" type="hidden" />
										<input type="submit" value="コメントを削除">
									</form>
								</c:if>
							</div>
						</c:if>
							</div>
						</c:forEach>
					</font>
				<div class="comment-form-area">
							<form action="newComment" method="post">
								<input name="postId" value="${post.id}" id="postId" type="hidden">
								<div class="writeComment"><b><font size="2">コメントを書く</font></b></div>
								<textarea name="comment" cols="40" rows="2" class="tweet-box"></textarea>
								<br />
								<input type="submit" value="コメント"><font size="2">(500文字まで)</font>
							</form>
						<br />
					</div>
					<hr>
				</c:forEach>
				</div>
			</div>

		<div class="copyright">
			Copyright(c) Kei Arisawa
		</div>
	</div>

</body>
</html>