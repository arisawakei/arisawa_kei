<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン画面</title>
</head>
<body>
	<div class="main-contents">
	<h1>ログイン画面</h1>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		<form action="login" method="post">
			<label for="account">ログインID</label>
			<input name="account" value="${loginUser.account}" id="account"/> <br />
			<label for="password">パスワード</label>
			<input name="password" type="password" id="password"/> <br />
			<div class="loginButton">
			<input type="submit" value="ログイン" /></div><br />
			<c:forEach items="${lists}" var="list">
			<input name="is_deleted" value= "${list.is_deleted}" id="is_deleted" type="hidden"/>
			</c:forEach>
			<br /><div class="copyright"> Copyright(c)Kei Arisawa</div>
		</form>
	</div>
</body>
</html>