<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<script type="text/javascript">
<!--

function check1(){
	if(window.confirm('停止しますか？')){
		return true;
	} else {
		window.alert('キャンセルしました');
		return false;
	}
}
function check2(){
	if(window.confirm('復活しますか？')){
		return true;
	} else {
		window.alert('キャンセルしました');
		return false;
	}
}
// -->
</script>
</head>
<body>
<div class="main-contents">
			<div class="menu">
			<a href ="http://localhost:8080/arisawa_kei/newPost">新規投稿</a>
			<c:if test = "${loginUser.branch_id ==1 && loginUser.department_id == 1}">
			<a href ="http://localhost:8080/arisawa_kei/manageUser">ユーザー管理</a>
			</c:if>
			<div class="log-out"><a href="logout">ログアウト</a></div>
			</div>
	<h1>ユーザー管理</h1>
	<a href = "./">ホーム</a> > ユーザー管理<br /><br />
		<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>
	<a href ="http://localhost:8080/arisawa_kei/signup"><b>新規ユーザー登録</b></a><br />
	<div class="posts">
		<table border="1">
		<tr>
		<td><div class ="table-margin"></div></td>
		<td><b>名前<div class ="table-margin"></div></b></td>
		<td><b>支店<div class ="table-margin"></div></b></td>
		<td><b>部署・役職<div class ="table-margin"></div></b></td>
		<td><b>状態<div class ="table-margin"></div></b></td>
		<td><b>管理<div class ="table-margin"></div></b></td>
		</tr>
		<c:forEach items="${lists}" var="list">
		<tr>
		<td>
		<a href ="./editUser?id=${list.id}"><b>編集</b></a>
		<td><c:out value="${list.name}" />
		</td>
		<td>
		<c:forEach items="${branchLists}" var="branchList">
		<c:if test="${list.branch_id == branchList.id}">
		<c:out value="${branchList.name}" />
		</c:if>
		</c:forEach>
		</td>
		<td>
		<c:forEach items="${departmentLists}" var="departmentList">
		<c:if test="${list.department_id == departmentList.id}">
		<div class="department_id"><c:out value="${departmentList.name}" /></div>
		</c:if>
		</c:forEach>
		</td>
		<td>
		<c:if test = "${list.is_deleted == 0}">
		<div class="is_deleted"><font color="blue"><c:out value="使用可" /></font></div>
		</c:if>
		<c:if test = "${list.is_deleted == 1}">
		<div class="is_deleted"><font color="red"><c:out value="停止中" /></font></div>
		</c:if>
		</td>
		<td>
		<c:if test="${list.is_deleted == 0 && loginUser.id != list.id}">
		<form action="manageUser" method="post" onSubmit="return check1()">
		<input name="id" value="${list.id}" id="id" type="hidden"/>
		<input name="is_deleted" value= "1" id="is_deleted" type="hidden"/>
		<input type="submit" value="停止">
		</form></c:if>
		<c:if test="${list.is_deleted == 1 && loginUser.id != list.id}">
		<form action="manageUser" method="post" onSubmit="return check2()">
		<input name="id" value="${list.id}" id="id" type="hidden"/>
		<input name="is_deleted" value= "0" id="is_deleted" type="hidden"/>
		<input type="submit" value="復活">
		</form>
		</c:if>
		<c:if test="${loginUser.id == list.id}">
		<div class="status">ログイン中</div>
		</c:if>
		</td>
		</tr></c:forEach>
		</table>
	</div>
			<div class="copyright">
			<br />Copyright(c) Kei Arisawa
		</div>
</div>
</body>
</html>