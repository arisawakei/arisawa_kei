<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div class="main-contents">
		<div class="menu">
			<a href="http://localhost:8080/arisawa_kei/newPost">新規投稿</a>
			<c:if
				test="${loginUser.branch_id ==1 && loginUser.department_id == 1}">
				<a href="http://localhost:8080/arisawa_kei/manageUser">ユーザー管理</a>
			</c:if>
			<div class="log-out"><a href="logout">ログアウト</a></div>
		</div>
		<h1>新規投稿</h1>
		<a href="./">ホーム</a> > 新規投稿<br />
		<br />
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="form-area">
			<form action="newPost" method="post">
				<label for="title">件名(30文字以内)</label><br /> <input name="title"
					value="${Post.title}" id="title" /> <br /> 本文(1000文字以内)<br />
				<textarea name="text" cols="40" rows="5" class="tweet-box">${Post.text}</textarea>
				<br /> <label for="category">カテゴリー(10文字以内)</label><br /> <input
					name="category" value="${Post.category}" id="category"><br />
				<br /> <input type="submit" value="投稿する">
			</form>
		</div>
	<div class="copyright">
		<br />Copyright(c) Kei Arisawa
	</div>
	</div>
</body>
</html>