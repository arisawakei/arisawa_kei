<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
				<div class="menu">
			<a href ="http://localhost:8080/arisawa_kei/newPost">新規投稿</a>
			<c:if test = "${loginUser.branch_id ==1 && loginUser.department_id == 1}">
			<a href ="http://localhost:8080/arisawa_kei/manageUser">ユーザー管理</a>
			</c:if>
			<div class="log-out"><a href="logout">ログアウト</a></div>
			</div>
	<h1>新規ユーザー登録</h1>
	<a href = "./">ホーム</a> > <a href = "./manageUser">ユーザー管理</a> > 新規ユーザー登録<br /><br />
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
		<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
		<div class="accountForm">
		<label for ="account">ログインID:</label>
		<input name ="account" value="${User.account}" type ="text">(6文字以上20文字以内)<br />
		</div>
		<div class="passwordForm">
		<label for ="password">パスワード:</label>
		<input name ="password" type ="password">(6文字以上20文字以内)<br />
		</div>
		<div class="passwordCheckForm">
		<label for ="passwordCheck">パスワード(確認用):</label>
		<input name ="passwordCheck" type ="password">(6文字以上20文字以内)<br />
		</div>
		<div class="nameForm">
		<label for ="name">名前:</label>
		<input name ="name" value="${User.name}" type="text">(10文字以内)<br />
		</div>

		<div class="branchForm">
		<label for ="branch_id">支店:</label>
		</div>
		<div class="selected">
		<select name = "branch_id">
		<c:forEach items="${branchLists}" var="branchList">
		<option value ="${branchList.id}" label= "${branchList.name}" <c:if test="${User.branch_id == branchList.id}">selected</c:if>  />
		</c:forEach>
		</select></div>

		<div class="departmentForm">
		<label for ="department_id">部署・役職:</label>
		</div>
		<div class="selected">
		<select name = "department_id">
		<c:forEach items="${departmentLists}" var="departmentList">
		<option value ="${departmentList.id}" label= "${departmentList.name}" <c:if test="${User.department_id == departmentList.id}">selected</c:if>  />
		</c:forEach>
		</select></div>

	    <div class="change"><input type ="submit" value="登録"><br /></div>
	    </form>
		<div class="copyright">
			<br />Copyright(c) Kei Arisawa
		</div>

	</div>
</body>
</html>