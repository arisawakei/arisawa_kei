-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: arisawa_kei
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'本社'),(2,'支店A'),(3,'支店B'),(4,'支店C');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (86,'コメントです','2018-04-18 00:52:40',45,56),(87,'aaa','2018-04-18 00:55:29',43,48),(88,'あああ','2018-04-18 01:05:52',43,48),(89,'ああ','2018-04-18 01:08:28',43,57),(90,'コメントです','2018-04-18 01:19:46',43,57),(91,'コメントです','2018-04-18 01:19:50',43,57),(92,'コメントですよ','2018-04-18 01:20:04',43,56),(95,'<br /><br /><br /><br /><br /><br />','2018-04-18 04:09:15',46,61),(97,'コメント１','2018-04-18 04:19:55',55,61),(100,'あ\r\nあ\r\nあ','2018-04-18 05:02:57',47,62),(101,'あいう','2018-04-18 06:24:54',47,62),(102,'あいう','2018-04-18 06:24:57',47,62),(103,'初コメ','2018-04-19 06:18:35',65,63),(104,'2ゲット','2018-04-19 06:19:06',66,63),(105,'インフラです。','2018-04-19 06:20:20',69,63),(107,'スペース投稿できないんですね。','2018-04-19 06:25:58',67,63),(108,'うおおおおおお','2018-04-19 06:26:30',68,63),(109,'コメントの改行\r\nコメントの改行','2018-04-19 06:32:03',71,67),(110,'aaa','2018-04-20 06:49:13',64,69),(111,'aaa','2018-04-20 06:49:14',64,69),(112,'werefwefwe','2018-04-20 06:49:16',64,69),(113,'wefwef','2018-04-20 06:49:18',64,69),(114,'ああ','2018-04-23 01:08:11',64,70),(115,'コメントです','2018-04-23 01:53:08',64,74),(116,'コメントテスト\r\nコメントテスト','2018-04-24 07:20:46',64,75),(117,'コメントテスト\r\n','2018-04-24 07:43:44',64,75),(118,'コメントテスト2\r\nコメントテスト2','2018-04-24 07:49:18',64,77),(119,'コメントテスト2','2018-04-24 07:49:23',64,77);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'総務人事'),(2,'情報管理'),(3,'支店長'),(4,'社員');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `category` varchar(10) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (75,'投稿','投稿テスト\r\n投稿テスト\r\n','テスト','2018-04-24 07:20:37',64),(77,'投稿テスト2','投稿テスト2\r\n投稿テスト2','テスト','2018-04-24 07:49:09',64);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (54,'admin01','CHbfym1v7fmbKrh7bi_tS9QFHt54qKkTW1ALLpTZm4g','名前w',1,2,1),(55,'admin02','JDpaT046J96GOR8HzwWKewtzzkYGm_hN90kwYphWD08','アドミン２',1,2,0),(57,'japanese','P271BMXSxXvbc0v7jN1RnlOn6X172nkE9fgyRjRrW9g','にほんご',3,4,0),(64,'arisawakei','n78mG2LB18ANtzr7gd2X_fILNELjbjOMuTWbhWoDvcg','ありさわけい',1,1,0),(65,'kawamuranaoki','xwUsUmYdLzajbsoo8HeBeih4IPPnVqCgQdhIxIclP3Q','川村直輝',1,2,0),(66,'tomitayuki','tn8wzSwr7Vd0G2X4kDCT4PY1PfVLnoD5s906h5UVvVk','冨田有輝',1,2,0),(67,'takizawayuki','osDZLbVY_T9-ovMeFcPL-GT6GDJxkZHeQbD-ju200tw','滝沢勇樹',1,2,0),(68,'takahashiosamu','2-IHL88S6VJ-y5bXZzNf_NETALqBiqvED7T-v3R98Ak','髙橋治',1,2,0),(69,'hiratsukaryota','BuO-revb02q7JmmB3XgIkQirR2GOo3RExu_hTfxQKg8','平塚遼汰',1,2,0),(70,'english','tiNNLqDWAivmPbgNe4DiIQl_5KRp3ET-vNKpJB7_3ro','えいご',2,4,0),(71,'javascript','7acXRsAcP0Zf_QK22hWmUY5vvI8G8axSW-GTvlUHBp0','ジャバスクリプト',4,3,1),(72,'ServletJSP','KYnxRUPwIKJGy8Ldw1tShg5ah9wXtNlGXWrw6BRiib8','サーブレットJSP',3,3,1),(73,'JQuery','y-2DljnXau6HO6_edPFsvcLLPSBpKYpSaZFGJhKS9dw','JQuery',4,4,0),(74,'programming','4IhnLTFXQTDZ8bCV3sorkBrzw-2RxJWMEcWrLOocCCo','プログラミング',2,3,0),(75,'Oracle','3HYg6_w11U7zTjK5629p8b_pPylDcMJ5jZEUfjTnrVY','Oracle',4,4,0),(76,'aaaaaa','7QJFe1xB2WTb0vKmCdY_4bt1KNvlXhq_W1LCSc1zV5c','aaaaaa',2,4,0),(77,'test001','lrjHOqnSvAxAIMLyZofEwBTQFTT7g6ZGCh2NccmvEGw','test001',2,3,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-27 15:22:15
