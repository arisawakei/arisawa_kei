package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.BranchList;
import beans.DepartmentList;
import beans.User;
import beans.UserList;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/editUser"})
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		User loginUser = (User) session.getAttribute("loginUser");

		List<UserList> list = new UserService().getList();

		if(request.getParameter("id") == null || StringUtils.isBlank(request.getParameter("id")) ||
				!request.getParameter("id").matches("^[0-9]+$")) {
			List<String> messages = new ArrayList<String>();
			messages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./manageUser");
			return;
		}

		User editUser = new UserService().editUser(Integer.parseInt(request.getParameter("id")));

		if(editUser == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./manageUser");
			return;
		}

		request.setAttribute("editUser", editUser);

		List<BranchList> branchList = new BranchService().selectBranch();
		List<DepartmentList> departmentList = new DepartmentService().selectDepartment();

		request.setAttribute("branchLists", branchList);
		request.setAttribute("departmentLists", departmentList);

		request.getRequestDispatcher("edituser.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
//		User editUser = getEditUser(request);
		User editUser = new UserService().editUser(Integer.parseInt(request.getParameter("id")));
		String account = request.getParameter("account");
		User accountCheck = UserService.account(account);
//		if(accountCheck != null && !editUser.getAccount().equals(request.getParameter("account")))
//			{
//
//			List<BranchList> branchList = new BranchService().selectBranch();
//			List<DepartmentList> departmentList = new DepartmentService().selectDepartment();
//
//			request.setAttribute("branchLists", branchList);
//			request.setAttribute("departmentLists", departmentList);
//
//			messages.add("アカウントが既に使用されています");
//			session.setAttribute("errorMessages", messages);
//			editUser = getEditUser(request);
//			request.setAttribute("editUser", editUser);
//
////			request.getRequestDispatcher("edituser.jsp").forward(request, response);
////			return;
//		}
		editUser = getEditUser(request);
		request.setAttribute("editUser", editUser);

		if(isValid(request, messages) == true) {
			try {
				new UserService().edit(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("edituser.jsp").forward(request, response);
				return;
			}
//			session.setAttribute("loginUser", editUser);
//
			response.sendRedirect("./manageUser");
		} else {
			session.setAttribute("errorMessages", messages);
			List<BranchList> branchList = new BranchService().selectBranch();
			List<DepartmentList> departmentList = new DepartmentService().selectDepartment();
//			branchList.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			request.setAttribute("branchLists", branchList);
			request.setAttribute("departmentLists", departmentList);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("edituser.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch_id(request.getParameter("branch_id"));
		editUser.setDepartment_id(request.getParameter("department_id"));

		return editUser;
	}



	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String name = request.getParameter("name");

		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
			}
		if(!StringUtils.isBlank(account) &&!account.matches("^[0-9a-zA-Z]+$")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if(!StringUtils.isBlank(account) && account.matches("^[0-9a-zA-Z]+$") && (account.length() < 6 || account.length() > 20)) {
			messages.add("ログインIDは6文字以上20文字以内で入力してください");
		}
		User editUser = new UserService().editUser(Integer.parseInt(request.getParameter("id")));

		User accountCheck = UserService.account(account);
		if(accountCheck != null && !editUser.getAccount().equals(request.getParameter("account")))
		{

		List<BranchList> branchList = new BranchService().selectBranch();
		List<DepartmentList> departmentList = new DepartmentService().selectDepartment();
		HttpSession session = request.getSession();
		request.setAttribute("branchLists", branchList);
		request.setAttribute("departmentLists", departmentList);

		messages.add("ログインIDが既に使用されています");
		session.setAttribute("errorMessages", messages);
//		editUser = getEditUser(request);
//		request.setAttribute("editUser", editUser);

//		request.getRequestDispatcher("edituser.jsp").forward(request, response);
//		return;
	}
		if (!StringUtils.isBlank(password) && !password.matches("^[\\x20-\\x7E]+$")){
			messages.add("パスワードは半角文字(記号も可)で入力してください");
		}
		if(!StringUtils.isBlank(password) && password.matches("^[\\x20-\\x7E]+$") && (password.length() < 6 || password.length() > 20)) {
			messages.add("パスワードは6文字以上20文字以内で入力してください");
		}
		if(!StringUtils.isBlank(password) && StringUtils.isBlank(passwordCheck) == true) {
			messages.add("パスワード(確認用)を入力してください");
		}
//		if(!StringUtils.isBlank(password) && !StringUtils.isBlank(passwordCheck) && StringUtils.equals(password, passwordCheck)) {
//			messages.add("パスワードが一致しません");
//		}
//		if(passwordCheck != "" && passwordCheck.length() > 20) {
//			messages.add("パスワード(確認用)は6文字以上20文字以内で入力してください");
//		}
//		if(passwordCheck != "" && passwordCheck.length() < 6) {
//			messages.add("パスワード(確認用)は6文字以上20文字以内で入力してください");
//		}
		if(!StringUtils.isBlank(password) && !StringUtils.isBlank(passwordCheck) && StringUtils.equals(password, passwordCheck) == false) {
			messages.add("パスワードが一致しません");
		}
		if(StringUtils.isBlank(name)){
			messages.add("名前を入力してください");
		}
		if(!StringUtils.isBlank(name) && name.length() > 10) {
			messages.add("名前は10文字以内で入力してください");
		}
		if (messages.size() == 0) {
           return true;
		} else {
           return false;
	}
	}
}

