//ホーム画面の仮実装

package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Post;
import beans.User;
import beans.UserComment;
import beans.UserList;
import beans.UserPost;
import service.CommentService;
import service.PostService;
import service.UserService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		List<UserList> list = new UserService().getList();
		request.setAttribute("lists", list);
//		request.setCharacterEncoding("UTF-8");
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		String category = request.getParameter("category");
		Post search = new Post();
		search.setFrom(request.getParameter("from"));
		search.setTo(request.getParameter("to"));
		search.setCategory(request.getParameter("category"));
		request.setAttribute("Search", search);


		if(to ==null && from==null) {
			from = "";
			to = "";
		}
		List<UserPost> post = new PostService().getPost(from, to, category);
		request.setAttribute("posts", post);
		List<UserComment> comment = new CommentService().getComment();
		request.setAttribute("comment", comment);

//		request.setAttribute("isShowMessageForm", isShowMessageForm);



		try {
			SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdfTo = new SimpleDateFormat("yyyy-MM-dd");
			List<String> messages = new ArrayList<String>();
			Date dateFrom = sdfFrom.parse(from);
			Date dateTo = sdfTo.parse(to);
			int compare = dateFrom.compareTo(dateTo);
			if(compare == 1) {
				messages.add("日付の形式が不正です");
				request.setAttribute("errorMessages", messages);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		request.getRequestDispatcher("home.jsp").forward(request, response);

	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		if(request.getParameter("id") != null) {
			int id = Integer.parseInt(request.getParameter("id"));
			new CommentService().delete(id);
			response.sendRedirect("./");
		}
		if(request.getParameter("deletePost") != null) {
			int deletePost = Integer.parseInt(request.getParameter("deletePost"));
			new PostService().delete(deletePost);
			response.sendRedirect("./");
		}
	}
}
