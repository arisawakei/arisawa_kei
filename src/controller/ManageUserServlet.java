package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BranchList;
import beans.DepartmentList;
import beans.User;
import beans.UserList;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

	@WebServlet(urlPatterns = { "/manageUser" })
	public class ManageUserServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		@Override
		protected void doGet(HttpServletRequest request,
				HttpServletResponse response) throws IOException, ServletException {

			User user = (User) request.getSession().getAttribute("loginUser");
			boolean isShowMessageForm;
			if (user != null) {
				isShowMessageForm = true;
			} else {
				isShowMessageForm = false;
			}
			List<UserList> list = new UserService().getList();
			request.setAttribute("lists", list);
			List<BranchList> branchList = new BranchService().selectBranch();
			List<DepartmentList> departmentList = new DepartmentService().selectDepartment();
//			branchList.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			request.setAttribute("branchLists", branchList);
			request.setAttribute("departmentLists", departmentList);

			request.setAttribute("isShowMessageForm", isShowMessageForm);

			request.getRequestDispatcher("/manageuser.jsp").forward(request, response);
		}
		@Override
		protected void doPost(HttpServletRequest request,
				HttpServletResponse response) throws IOException, ServletException {

			String id = request.getParameter("id");
			String flg = request.getParameter("is_deleted");

			new UserService().setDeleted(id, flg);
			response.sendRedirect("./manageUser");
		}

	}