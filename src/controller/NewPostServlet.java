package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("newpost.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if(isValid(request, messages) == true) {
			User user = (User) session.getAttribute("loginUser");

			Post post = new Post();
			post.setTitle(request.getParameter("title"));
			post.setText(request.getParameter("text"));
			post.setCategory(request.getParameter("category"));
			post.setUserId(user.getId());

			new PostService().register(post);
			response.sendRedirect("./");

		} else {

			User user = (User) session.getAttribute("loginUser");
			Post post = new Post();
			post.setTitle(request.getParameter("title"));
			post.setText(request.getParameter("text"));
			post.setCategory(request.getParameter("category"));
			post.setUserId(user.getId());
			request.setAttribute("Post", post);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if(StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}
		if(30 < title.length()) {
			messages.add("件名は30文字以内で入力してください");
		}
		if(StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}
		if(1000 < text.length()) {
			messages.add("本文は1000文字以内で入力してください");
		}
		if(StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if(10 < category.length()) {
			messages.add("カテゴリーは10文字以内で入力してください");
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
