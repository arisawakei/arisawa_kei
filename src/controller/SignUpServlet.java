package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.BranchList;
import beans.DepartmentList;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<BranchList> branchList = new BranchService().selectBranch();
		List<DepartmentList> departmentList = new DepartmentService().selectDepartment();

		request.setAttribute("branchLists", branchList);
		request.setAttribute("departmentLists", departmentList);

		request.getRequestDispatcher("signup.jsp").forward(request,  response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();

		String account = request.getParameter("account");
		User accountCheck = UserService.account(account);
		if(accountCheck != null)
			{
			HttpSession session = request.getSession();

			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setDepartment_id(request.getParameter("department_id"));
			user.setIs_deleted("0");
			request.setAttribute("User", user);
			List<BranchList> branchList = new BranchService().selectBranch();
			List<DepartmentList> departmentList = new DepartmentService().selectDepartment();
//			branchList.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			request.setAttribute("branchLists", branchList);
			request.setAttribute("departmentLists", departmentList);

//			session.setAttribute("errorMessages", messages);
//			request.getRequestDispatcher("signup.jsp").forward(request, response);
//			return;
		}

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setDepartment_id(request.getParameter("department_id"));
			user.setIs_deleted("0");
			request.setAttribute("User", user);

			new UserService().register(user);
			response.sendRedirect("./manageUser");
		} else {

			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setDepartment_id(request.getParameter("department_id"));
			user.setIs_deleted("0");
			request.setAttribute("User", user);
			List<BranchList> branchList = new BranchService().selectBranch();
			List<DepartmentList> departmentList = new DepartmentService().selectDepartment();
//			branchList.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			request.setAttribute("branchLists", branchList);
			request.setAttribute("departmentLists", departmentList);

			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String name= request.getParameter("name");

//		if(UserService.account(account) == true ) {
//			messages.add("アカウントは既に使用されています");
//			return false;
//		}
		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		}
		if(!account.matches("^[0-9a-zA-Z]+$") && !StringUtils.isBlank(account)) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if(!StringUtils.isBlank(account) && (account.length() < 6 || account.length() > 20)) {
			messages.add("ログインIDは6文字以上20文字以内で入力してください");
		}
		User accountCheck = UserService.account(account);
		if(accountCheck != null && !StringUtils.isBlank(account))
			{
			HttpSession session = request.getSession();

			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setDepartment_id(request.getParameter("department_id"));
			user.setIs_deleted("0");
			request.setAttribute("User", user);
			List<BranchList> branchList = new BranchService().selectBranch();
			List<DepartmentList> departmentList = new DepartmentService().selectDepartment();
			request.setAttribute("branchLists", branchList);
			request.setAttribute("departmentLists", departmentList);

			messages.add("ログインIDが既に使用されています");
			session.setAttribute("errorMessages", messages);
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (!StringUtils.isBlank(password) && !password.matches("^[\\x20-\\x7E]+$")){
			messages.add("パスワードは半角文字(記号も可)で入力してください");
		}
		if(!StringUtils.isBlank(password) && password.matches("^[\\x20-\\x7E]+$") && (password.length() < 6 || password.length() > 20)) {
			messages.add("パスワードは6文字以上20文字以内で入力してください");
		}
		if(!StringUtils.isBlank(password) && StringUtils.isBlank(passwordCheck) == true) {
			messages.add("パスワード(確認用)を入力してください");
		}
		if(!StringUtils.isBlank(password) && !StringUtils.isBlank(passwordCheck) && StringUtils.equals(password, passwordCheck) == false) {
			messages.add("パスワードが一致しません");
		}
		if(StringUtils.isBlank(name)){
			messages.add("名前を入力してください");
		}
		if(!StringUtils.isBlank(name) && name.length() > 10) {
			messages.add("名前は10文字以内で入力してください");
			return false;
		}
		if (messages.size() == 0) {
			return true;
	    } else {
			return false;
		}
	}

}
