package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.BranchList;
import exception.SQLRuntimeException;

public class BranchSelectDao {

	public static List<BranchList> selectBranch(Connection connection) {

	PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM branches ";
			ps = connection.prepareStatement(sql.toString());
			ps.executeQuery();


			ResultSet rs = ps.executeQuery();
			List<BranchList> branchList = toBranchList(rs);
			if(branchList.isEmpty() == true) {
				return null;
			} else {
				return branchList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException (e);
		} finally {
			close(ps);
		}

	}
	private static List<BranchList> toBranchList(ResultSet rs) throws SQLException {
		List<BranchList> ret = new ArrayList<BranchList>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				BranchList user = new BranchList();
				user.setId(id);
				user.setName(name);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
			}
	}
}