package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("text");
			sql.append(", created_date");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(") VALUES (");
			sql.append(" ?"); //text
			sql.append(", CURRENT_TIMESTAMP"); //created_date
			sql.append(", ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Comment> toCommentList(ResultSet rs) throws SQLException {
		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String text = rs.getString("text");
				Timestamp created_date = rs.getTimestamp("created_date");
				int userId = rs.getInt("userId");
				int postId = rs.getInt("postId");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setText(text);
				comment.setCreated_date(created_date);
				comment.setUserId(userId);
				comment.setPostId(postId);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public Comment deleteComment(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<Comment> commentList = toCommentList(rs);
			if (commentList.isEmpty() == true) {
				return null;
			} else if (2 <= commentList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return commentList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException (e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments");
			sql.append(" WHERE");
			sql.append(" id=?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
