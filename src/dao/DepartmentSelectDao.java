package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.DepartmentList;
import exception.SQLRuntimeException;

public class DepartmentSelectDao {
	public static List<DepartmentList> selectDepartment(Connection connection) {

		PreparedStatement ps = null;

			try {
				String sql = "SELECT * FROM departments ORDER BY name ";
				ps = connection.prepareStatement(sql.toString());
				ps.executeQuery();


				ResultSet rs = ps.executeQuery();
				List<DepartmentList> departmentList = toDepartmentList(rs);
				if(departmentList.isEmpty() == true) {
					return null;
				} else {
					return departmentList;
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException (e);
			} finally {
				close(ps);
			}

		}
		private static List<DepartmentList> toDepartmentList(ResultSet rs) throws SQLException {
			List<DepartmentList> ret = new ArrayList<DepartmentList>();
			try {
				while(rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");

					DepartmentList user = new DepartmentList();
					user.setId(id);
					user.setName(name);

					ret.add(user);
				}
				return ret;
			} finally {
				close(rs);
				}
		}

}
