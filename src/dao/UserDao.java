package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserList;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append("?"); //account
			sql.append(", ?"); //password
			sql.append(", ?"); //name
			sql.append(", ?"); //branch_id
			sql.append(", ?"); //department_id
			sql.append(", ?"); //is_deleted
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			int intBranch = Integer.parseInt(user.getBranch_id());
			int intDepartment = Integer.parseInt(user.getDepartment_id());
			ps.setInt(4, intBranch);
			ps.setInt(5, intDepartment);
			ps.setString(6, "0");
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public User getUser(Connection connection, String account, String password) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE (account = ?) AND password = ? AND is_deleted = 0";
			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<User>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branch_id = rs.getString("branch_id");
				String department_id = rs.getString("department_id");
				String is_deleted = rs.getString("is_deleted");

				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setDepartment_id(department_id);
				user.setIs_deleted(is_deleted);

				ret.add(user);
			}
			return ret;
		} finally {
		close(rs);
		}
	}


	public boolean delete(Connection connection, String id, String flg)
			throws SQLException{
		PreparedStatement ps = null;
		String sql = "UPDATE users SET is_deleted = ? WHERE id = ?";
		ps = connection.prepareStatement(sql);


		ps.setString(1, flg);
		ps.setString(2, id);

		int i = ps.executeUpdate();

		return true;
	}
	public void update(Connection connection, User manage)
			throws SQLException{
		PreparedStatement ps = null;
		String sql = "UPDATE users SET is_deleted = ? WHERE id = ?";
		ps = connection.prepareStatement(sql.toString());
		int cast = Integer.parseInt(manage.getIs_deleted());
		ps.setInt(1, cast);
		ps.setInt(2, manage.getId());
		int i = ps.executeUpdate(sql);
	}


	public User editUser(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if ( userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException (e);
		} finally {
			close(ps);
		}
	}
	public void edit(Connection connection, User editUser) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			if(!StringUtils.isBlank(editUser.getPassword())) {
				sql.append("UPDATE users SET");
				sql.append(" account = ?");
				sql.append(", password = ?");
				sql.append(", name = ?");
				sql.append(", branch_id = ?");
				sql.append(", department_id = ?");
	            sql.append(" WHERE");
	            sql.append(" id = ?");
			} else {
				sql.append("UPDATE users SET");
				sql.append(" account = ?");
				sql.append(", name = ?");
				sql.append(", branch_id = ?");
				sql.append(", department_id = ?");
	            sql.append(" WHERE");
	            sql.append(" id = ?");
			}

			ps = connection.prepareStatement(sql.toString());

			if(!StringUtils.isBlank(editUser.getPassword())) {
				ps.setString(1, editUser.getAccount());
				ps.setString(2, editUser.getPassword());
				ps.setString(3, editUser.getName());
				ps.setString(4, editUser.getBranch_id());
				ps.setString(5, editUser.getDepartment_id());
				ps.setInt(6, editUser.getId());
			} else {
				ps.setString(1, editUser.getAccount());
				ps.setString(2, editUser.getName());
				ps.setString(3, editUser.getBranch_id());
				ps.setString(4, editUser.getDepartment_id());
				ps.setInt(5, editUser.getId());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<UserList> getUserLists(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users ORDER BY branch_id, department_id";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserList> ret = toUserListList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserList> toUserListList(ResultSet rs)
			throws SQLException {

		List<UserList> ret = new ArrayList<UserList>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int department_id = rs.getInt("department_id");
				int is_deleted = rs.getInt("is_deleted");

				UserList list = new UserList();
				list.setId(id);
				list.setAccount(account);
				list.setName(name);
				list.setBranch_id(branch_id);
				list.setDepartment_id(department_id);
				list.setIs_deleted(is_deleted);

				ret.add(list);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public User existAccount(Connection connection, String account) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
