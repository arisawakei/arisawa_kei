package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserPost;
import exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPosts(Connection connection, int num, String from, String to, String category) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.text as text, ");
			sql.append("posts.category as category, ");
			sql.append("posts.created_date as created_date, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("users.name as name ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE created_date >= ? AND created_date <= ? ");
			if(!StringUtils.isEmpty(category)) {
				sql.append("AND posts.category LIKE ? ");
			}
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			System.out.println(ps);

			ps.setString(1, from + " 00:00:00");
			ps.setString(2, to + " 23:59:59");
			if(!StringUtils.isEmpty(category)) {
				ps.setString(3, "%" + category + "%");
			}

			System.out.println(ps);

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				int id =rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp created_date = rs.getTimestamp("created_date");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");


				UserPost post = new UserPost();
				post.setId(id);
				post.setTitle(title);
				post.setText(text);
				post.setCategory(category);
				post.setCreated_date(created_date);
				post.setUserId(userId);
				post.setName(name);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}