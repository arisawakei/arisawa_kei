package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns={"/editUser", "/signup", "/manageUser"})

public class AuthorityFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response,
	    FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession(false);
//		request.getAttribute("loginUser" );
		List<String> messages = new ArrayList<String>();

		if(session == null){
			messages.add("ログインしてください");
			session = ((HttpServletRequest)request).getSession();
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./login");
			return;
		}

		if (session.getAttribute("loginUser") == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./login");
			return;
		}

		User user = (User)session.getAttribute("loginUser");

		String branch_id = user.getBranch_id();
		String department_id = user.getDepartment_id();
		if (branch_id.equals("1") && department_id.equals("1")) {
			chain.doFilter(request, response);
//		} else if(StringUtils.isEmpty(branch_id) && StringUtils.isEmpty(department_id)) {
//			((HttpServletResponse)response).sendRedirect("./login");
		} else {
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./");

		}

	}
	public void init(FilterConfig config) {
	}

	public void destroy() {
    }


}
