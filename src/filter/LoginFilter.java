package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns={"/editUser", "/index.jsp", "/login", "/logout", "/manageUser", "/newComment", "/newPost", "/signup"})

public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
	    FilterChain chain) throws IOException, ServletException {
		HttpSession session = ((HttpServletRequest)request).getSession(false);
		String servletPath = (((HttpServletRequest) request).getServletPath());
		List<String> messages = new ArrayList<String>();

		if("/login".equals(servletPath)) {
			chain.doFilter(request, response);
		} else if(session == null){
			messages.add("ログインしてください");
			session = ((HttpServletRequest)request).getSession();
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./login");
			return;
		} else if(session.getAttribute("loginUser") == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./login");
			return;
		} else {
			chain.doFilter(request, response);
		}

	}
	public void init(FilterConfig config) {
	}

	public void destroy() {
    }
}
