package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.BranchList;
import dao.BranchSelectDao;

public class BranchService {
	Connection connection = null;
	public List<BranchList> selectBranch() {
		try {
			connection = getConnection();

			BranchSelectDao branchSelectDao = new BranchSelectDao();
			List<BranchList> branch = branchSelectDao.selectBranch(connection);

			commit(connection);

			return branch;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
