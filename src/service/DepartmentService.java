package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.DepartmentList;
import dao.DepartmentSelectDao;

public class DepartmentService {
	Connection connection = null;
	public List<DepartmentList> selectDepartment() {
		try {
			connection = getConnection();

			DepartmentSelectDao DepartmentSelectDao = new DepartmentSelectDao();
			List<DepartmentList> department = DepartmentSelectDao.selectDepartment(connection);

			commit(connection);

			return department;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
