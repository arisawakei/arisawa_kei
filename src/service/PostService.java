package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import beans.Post;
import beans.UserPost;
import dao.PostDao;
import dao.UserPostDao;

public class PostService {
	public void register(Post post) {
		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public static final int LIMIT_NUM = 1000;

	public List<UserPost> getPost(String from, String to, String category) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserPostDao postDao = new UserPostDao();
			if(from.equals("") && to.equals("")) {
				from = "2010-01-01";
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				String str = sdf1.format(new Date());
				to = str;
			}
			System.out.println(from);
			System.out.println(to);
			List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, from, to, category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public boolean delete(int deletePost) {
		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.delete(connection, deletePost);
			commit(connection);
			return true;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}
